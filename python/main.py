import socket
import sys

from CentriPetalBalancerBot import CentriPetalBalancerBot

if __name__ == "__main__":
    if len(sys.argv) != 6:
        print("Usage: ./run host port botname track botkey")
    else:
        host, port, name, track, key = sys.argv[1:6]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, track={3}, key={4}".format(*sys.argv[1:6]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))

        bot = CentriPetalBalancerBot(s, name, track, key)
        bot.run()
