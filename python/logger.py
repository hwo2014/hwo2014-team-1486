import sys

__message_delim__ = "\n----\n"

class Logger(object):
    def __init__(self, time_string):
	self.file = open("logs/" + time_string + ".txt", "w+")

    def log(self, msg):
	self.file.write(msg + __message_delim__)
