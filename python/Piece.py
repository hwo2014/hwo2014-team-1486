class Piece(object):
    @staticmethod
    def factory(piece):
        if 'length' in piece: return Straight(piece)
        return Bend(piece)

    def isStraight(self):
        return type(self) is Straight

class Straight(Piece):
    def __init__(self, piece):
        self.length = piece['length']
        self.switch = False
        if 'switch' in piece:
            self.switch = piece['switch']

class Bend(Piece):
    def __init__(self, piece):
        self.angle = piece['angle']
        self.radius = piece['radius']
