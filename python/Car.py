class Car(object):
    def __init__(self, car):
        self.name = car['id']['name']
        self.color = car['id']['color']

        dimensions = car['dimensions']
        self.length = dimensions['length']
        self.width = dimensions['width']
        self.guideFlagPosition = dimensions['guideFlagPosition']

        self.lane = -1
        self.angle = 45.0
        self.pieceIndex = 0
        self.inPieceDistance = 0
        self.lap = -1

        self.velocity = 0

    def update(self, carPosition):
        prevPos = self.inPieceDistance
        prevIndex = self.pieceIndex

        self.angle = carPosition['angle']

        piecePosition = carPosition['piecePosition']
        self.lap = piecePosition['lap']
        self.pieceIndex = piecePosition['pieceIndex']
        self.inPieceDistance = piecePosition['inPieceDistance']
        self.lane = piecePosition['lane']['endLaneIndex']

        if self.pieceIndex != prevIndex:
            self.velocity = self.inPieceDistance  # this is not right
        else:
            self.velocity = self.inPieceDistance - prevPos

    # We assume for simplicity that mass is proportional to car area
    def mass(self):
        return self.length * self.width
