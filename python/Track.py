from Piece import Piece
from Lane import Lane

class Track(object):
    def __init__(self, track):
        self.id = track['id'];
        self.name = track['name'];
        self.startingPoint = track['startingPoint']
        self.lanes = [Lane(lane) for lane in track['lanes']]

        self.pieces = [Piece.factory(piece) for piece in track['pieces']]

    # Returns the radius of the given piece for the given lane or a big value if the piece is
    # not a bend.
    def radius(self, pieceIndex, laneIndex):
		piece = self.pieces[pieceIndex]

		if piece.isStraight():
			return 9999999.0  # TODO: Prettier value? No radius
		else:
			r = piece.radius
			if piece.angle < 0.0:
				return r + self.lanes[laneIndex].distanceFromCenter
			else:
				return r - self.lanes[laneIndex].distanceFromCenter
