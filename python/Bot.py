import json
from Car import Car
from Track import Track

from pprint import pprint

class Bot(object):
    def __init__(self, socket, name, trackName, key):
        self.socket = socket
        self.name = name
        self.trackName = trackName
        self.key = key

        self.track = None
        self.cars = {}

        self.myColor = ''
        self.myCar = None

        self.laps = -1
        self.maxLapTimeMs = -1
        self.quickRace = False

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def joinRace(self):
        return self.msg("joinRace", {"botId": {"name": self.name,
           "key": self.key},
           "carCount": 1,
           "trackName": self.trackName})

    def join(self):
        return self.msg("join", {"name": self.name,
           "key": self.key})

    def throttle(self, throttle):
        self.myCar.throttle = throttle
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
#        self.joinRace()
        self.get_messages()

    ### on messages
    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_joinRace(self, data):
        print("JoinRaced")
        self.ping()

    def on_gameStart(self, data):
        print("bot: on_gameStart")
        self.ping()

    # Override this!
    def on_carPositions(self, data):
        self.updateCars(data)
        self.throttle(1)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_gameEnd(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_yourCar(self, data):
        print('bot: on_yourCar')
        self.myColor = data['color']
        self.ping()

    def on_gameInit(self, data):
        print('bot: on_gameInit')

        race = data['race']
        self.track = Track(race['track'])
        self.initCars(race['cars'])

        # raceSession = race['raceSession']
        # self.laps = raceSession['laps']
        # self.maxLapTimeMs = raceSession['maxLapTimeMs']
        # self.quickRace = raceSession['quickRace']

        self.ping()
    ### /on messages

    def updateCars(self, cars):
        for car in cars:
            self.updateCar(car)

    def updateCar(self, car):
        self.cars[car['id']['color']].update(car)

    def initCar(self, car):
        if (self.myColor == car['id']['color']):
            newCar = Car(car)
            self.myCar = newCar
        else:
            newCar = Car(car)

        self.cars[car['id']['color']] = newCar

    def initCars(self, cars):
        for car in cars:
            self.initCar(car)

    ## message loops
    def get_messages(self):
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            self.got_message(msg_type, data)
            line = socket_file.readline()

    def got_message(self, type, data):
        try:
            methodName = "on_" + type
            method = getattr(self, methodName)
            method(data)
        except AttributeError:
            print("Unhandled message: {0}".format(type))
            self.ping()
    ## /message loops
