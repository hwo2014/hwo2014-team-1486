import socket
import json
import sys

def send_message(msg_type, data):
    print("Trying to send:")
    print(json.dumps({"msgType": msg_type, "data": data}) + "\n")
    client.send(json.dumps({"msgType": msg_type, "data": data}) + "\n")

def send_yourcar():
    send_message("yourCar", {"name": botname,
        "color": "red"})

# Game init from tech spec
def send_game_init():
    gameInitData = {
      "race": {
        "track": {
          "id": "indianapolis",
          "name": "Indianapolis",
          "pieces": [
            {
              "length": 100.0
            },
            {
              "length": 100.0,
              "switch": True
            },
            {
              "radius": 200,
              "angle": 22.5
            }
          ],
          "lanes": [
            {
              "distanceFromCenter": -20,
              "index": 0
            },
            {
              "distanceFromCenter": 0,
              "index": 1
            },
            {
              "distanceFromCenter": 20,
              "index": 2
            }
          ],
          "startingPoint": {
            "position": {
              "x": -340.0,
              "y": -96.0
            },
            "angle": 90.0
          }
        },
        "cars": [
          {
            "id": {
              "name": botname,
              "color": "red"
            },
            "dimensions": {
              "length": 40.0,
              "width": 20.0,
              "guideFlagPosition": 10.0
            }
          }
        ],
        "raceSession": {
          "laps": 3,
          "maxLapTimeMs": 30000,
          "quickRace": True
        }
      }
    }
    send_message("gameInit", gameInitData)

def send_game_start():
    send_message("gameStart", {})

def send_car_positions(gametick):
    carPositionsData = [
        {
            "id": {
                "name": botname,
                "color": "red"
            },
            "angle": 0.0,
            "piecePosition": {
                "pieceIndex": 0,
                "inPieceDistance": 0.0,
                "lane": {
                    "startLaneIndex": 0,
                    "endLaneIndex": 0
                },
                "lap": 0
            }
        }
    ]

    print("Sending car positions")
    print("")
    client.send(json.dumps({"msgType": "carPositions", "data": carPositionsData, "gameId": 0, "gameTick": gametick}) + "\n")

def send_gameend():
    gameEndData = {
        "results": [
        {
            "car": {
                "name": botname,
                "color": "red"
            },
            "result": {
                "laps": 3,
                "ticks": 9999,
                "millis": 45245
            }
        }
        ],
        "bestLaps": [
        {
            "car": {
                "name": "Schumacher",
                "color": "red"
            },
            "result": {
                "lap": 2,
                "ticks": 3333,
                "millis": 20000
            }
        }
        ]
    }
    send_message("gameEnd", gameEndData)

def send_tournamentend():
    send_message("tournamentEnd", {})

def on_update(state, data):
    if state == 0:
        global botname
        botname = data["name"]
        send_yourcar()
        return 1
    if state == 1:
        send_game_init()
        return 2
    elif state == 2:
        send_game_start()
        return 3
    elif state < 10:
        send_car_positions(state)
        return state+1
    elif state == 10:
        send_gameend()
        return 11
    elif state == 11:
        send_tournamentend()
        return 12
    print("Finished!")
    return sys.exit(0)

def handle_msg(msg, state):
    print("Received:")
    print(msg)
    print("")

    msg_type, data = msg['msgType'], msg['data']
    return on_update(state, data)

if len(sys.argv) != 2:
    print("Usage: python test_server.py port")
    sys.exit(1)


host = ''  # Localhost
port = int(sys.argv[1])
backlog = 5
size = 1024

state = 0

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((host,port))
s.listen(backlog)
client, address = s.accept()
while 1:
    msg = json.loads(client.recv(2048))
    if msg:
        state = handle_msg(msg, state)
