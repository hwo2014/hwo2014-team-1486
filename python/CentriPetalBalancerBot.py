import math
import sys

from Bot import Bot
from Car import Car
from Track import Track

CENTRIPETAL_FORCE_MAX = 3.60

def centriPetalForce(mass, velocity, radius):
    return (mass * velocity * velocity) / radius

class CentriPetalBalancerBot(Bot):
	def on_lapFinished(self, data):
		print("Lap finished: " + str(data['lapTime']['millis']))


	def on_carPositions(self, data):
		self.updateCars(data)

		velocity = self.myCar.velocity / 10.0

		radius1 = self.track.radius(self.myCar.pieceIndex, self.myCar.lane)
		radius2 = self.track.radius( (self.myCar.pieceIndex+1) % len(self.track.pieces), self.myCar.lane)
		radius = min(radius1, radius2)

		if radius < 9999999.0:
			force = centriPetalForce(self.myCar.mass(), velocity, radius)

			if force >= CENTRIPETAL_FORCE_MAX:
				self.throttle(0.0)
				return

		self.throttle(1.0)
